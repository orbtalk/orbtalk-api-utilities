import pyfscache
import os
from openapi_codec import OpenAPICodec
from coreapi.transports import HTTPTransport
from hal_codec import HALCodec
from coreapi.client import Client
from coreapi_problem_json import ProblemJSON
from urlparse import urlparse
import requests
import json


cache_dir = './cache'
if not os.path.exists(cache_dir):
    os.makedirs(cache_dir)

cache_it = pyfscache.FSCache(cache_dir, minutes=30)


@cache_it
def get_token(auth_host, auth_username, auth_password):
    payload = "grant_type=password&username={0}&password={1}".format(auth_username,
                                                                     auth_password)
    response = requests.request("POST", auth_host, data=payload, headers={
        'content-type': "application/x-www-form-urlencoded"
    })
    response.raise_for_status()
    response_dict = json.loads(response.text)
    return response_dict["access_token"]


def _parse_host(auth_endpoint):
    urls_parts = urlparse(auth_endpoint)
    return urls_parts.hostname


def setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url):
    decoders = [OpenAPICodec(), HALCodec(), ProblemJSON()]
    token = get_token(auth_endpoint, auth_username, auth_password)
    host = _parse_host(schema_url)
    credentials = {
        host: "Bearer {0}".format(token)
    }
    transports = [HTTPTransport(credentials=credentials)]
    client = Client(decoders=decoders, transports=transports)
    schema = client.get(schema_url, force_codec='openapi')
    return client, schema
